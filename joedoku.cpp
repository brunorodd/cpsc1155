// Assignment 6-1
/*
Problem: Build a Joedoku game that play likes sudoku but with letters.

*/
// Author: Bruno Rodriguez
// Date: November 29th 2016

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <string>
using namespace std;
const int arrSize = 10;

void drawBoard(char matrix[][arrSize]);
void drawLineAcross();
void drawNewSetLineAcross();



// step 3: initialize board function that fills first row and column with numbers 1 through 9
void initializeBoard(char board[][10])
{
    //initializing first column
    board[0][1] = '1';
    board[0][2] = '2';
    board[0][3] = '3';
    board[0][4] = '4';
    board[0][5] = '5';
    board[0][6] = '6';
    board[0][7] = '7';
    board[0][8] = '8';
    board[0][9] = '9';
    
    // initializing first row
    board[1][0] ='1';
    board[2][0] ='2';
    board[3][0] ='3';
    board[4][0] ='4';
    board[5][0] ='5';
    board[6][0] ='6';
    board[7][0] ='7';
    board[8][0] ='8';
    board[9][0] ='9';
}

//step 4
void announceProgram()
{
    cout <<"Start of Program, Assign6-1.cpp!!!!!!!!"<<endl;
    cout <<"----------------------------------------"<<endl;
    cout <<"Welcome to Joedoku! "<<endl;
    cout <<"Description of the game: "<< endl;
    cout <<"Each puzzle consists of a 9x9 Sudoku grid"<< endl;
    cout <<"containing areas surrounded by gray or dotted"<<endl;
    cout <<"lines. The object is to fill all empty squares"<<endl;
    cout <<"so that the letters A to I appear exactly once"<<endl;
    cout <<"in each row, column and 3x3 box, and the sum of"<<endl;
    cout <<"the letters in each area is equal to the clue " <<endl;
    cout <<"in the area's top-left corner."<<endl;
    
    
    
}

// step 5
char getLevel()
{
    char input;
    cout <<"Enter a level to play! (E = Easy, M = Medium, H = Hard): ";
    cin>> input;
    
    // returns the level from the input of the user
    return input;

    
}

bool validateMove(string move, char board[][arrSize])
{
    bool uniqueMove = true;
    int row = move[0] -48, column = move[1] -48; char posMove = move[2];
    //chekc row
    for(int i = 1; i<arrSize; i++){
        if(board[row][i] == posMove){
            uniqueMove = false;
        }
    }
    
    //check column
    for(int j = 1; j<arrSize; j++){
        if(board[j][column] == posMove){
            uniqueMove = false;
        }
    }
    
    //check square
    int x = ((row-1)/3)*3+1;
    int y = ((column-1)/3)*3+1;
    for (int i = x; i < x+3; i++)
    {
      for (int j = y; j <y+3; j++)
      {
          if (board[i][j] == posMove)
          {
              uniqueMove = false;
         }
          
      }
    }
    return uniqueMove;
}


bool validateInput(string move, char board[][arrSize])
{
    
    if ((move[0] >= '1' && move[0] <= '9') && (move[1] >= '1' && move[1] <='9'))
    {
    if (move[2] <='J' && move[2]>= 'A')
    {
        if (board[move[0]-48][move[1]-48] == ' ')
        {
            if (validateMove(move, board)){
            return true;
            }
            else 
            return false;
            
        }
        else
            return false;
    }
    else
    {
        return false;
    }
    }
    else
        return false;
    // check row, column, and square
    
}

// step 6
void fillBoard(char level, char board[][arrSize])
{
    char inputChar;
    fstream input;
    
    //input for easy file
if (level == 'E')
{
    input.open("easypuzzle.txt");
}
   
    // input open for medium text file
if (level == 'M')
{
    input.open("mediumpuzzle.txt");
}
    
    // open file for hard text file
else
{
    input.open("hardpuzzle.txt");
}
    
    for (int row = 1; row < arrSize; row++) {
        for (int col = 1; col < arrSize; col++) {
            input >> inputChar;
            if (inputChar == 'J')
                inputChar = ' ';
            board[row][col] = inputChar;
        }
    }
    input.close();  // closes the file
}
// fill solution function
void fillSolution(char level, char board[][arrSize])
{
 char inputChar;
    fstream input;
    
    //input for easy file
if (level == 'E')
{
    input.open("easypuzzleanswer.txt");
        cout<<"Congratulations!"<<endl;
}
   
    // input open for medium text file
if (level == 'M')
{
    input.open("mediumpuzzleanswer.txt");
        cout<<"Congratulations!"<<endl;
}
    
    // open file for hard text file
else
{
    input.open("hardpuzzleanswer.txt");
    cout<<"Congratulations!"<<endl;
}
    
    for (int row = 1; row < arrSize; row++) {
        for (int col = 1; col < arrSize; col++) {
            input >> inputChar;
            if (inputChar == 'J')
                inputChar = ' ';
            board[row][col] = inputChar;
        }
    }
    input.close();  // closes the file
}



string getMove(char board[][arrSize])
{
    bool x = false;
    bool finish = false;
    while(!finish){
        string s = "";
        cout << "Please enter a move" << endl;
        cin >> s;
    
        //check if valid move
        x = validateInput(s, board);
        if (x == true)
        return s;
    }  
    
    
}
// Step 7e - Draw Board
void drawBoard(char matrix[][arrSize]) {
    
    printf("\n----------------\nCurrent Board\n----------------\n");
    
    // Show the Row and Col IDs
    for (int col = 0; col <= arrSize-1; col++) {
        if (col == 0)
            printf("     ");
        else
            printf("\t%1c  ",matrix[0][col]);
    }
    printf("\n");
    
    drawNewSetLineAcross();
    
    for (int row = 1; row <= arrSize-1; row++) {
        
        printf("  %1c |",matrix[row][0]);
        for (int col = 1; col <=arrSize-1; col++) {
            printf("|\t%1c   ",matrix[row][col]);
            if (col % 3 == 0) {
                printf("|");
            }
        }
        printf("|\n");
        if (row % 3 == 0)
            drawNewSetLineAcross();
        else
            drawLineAcross();
    }
    
    printf("\n\n");
}

void drawLineAcross() {
    printf("    --------------------------------------------------------------------------\n");
}

void drawNewSetLineAcross() {
    printf("    ==========================================================================\n");
}



// placeMove
void placeMove(char board[][arrSize])
{
    string x = getMove(board);
    
    board[x[0]-48][x[1]-48] = x[2];

}

\bool finishPuzzle()
// main function
int main()
{
    // initializing the sudoku board array
    char sudokuBoard[10][10];
    
    // function call to announce program
    announceProgram();
    
    // function call to initialize board
    initializeBoard(sudokuBoard);
    
    // retrieve level from function call to getLevel
    char input = getLevel(); // input is the level

    // call the fillBoard function to fill the board
    fillBoard(input, sudokuBoard);

    // print the board
    bool endGame = false;
    while (!endGame)
    {
    drawBoard(sudokuBoard);
    placeMove(sudokuBoard);
    
    }


    
    return 0;
    
  
    
}
